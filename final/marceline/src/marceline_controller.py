#!/usr/bin/env python

import rospy
from Tkinter import *
from marceline.srv import Control
import os

os.system('xset r off')

node_name = 'marceline_controller'
service_name = 'keyboard'

class Controller(Tk):
    def __init__(self):
        
        rospy.init_node(node_name)
        #rospy.wait_for_service(service_name)      
        self.controller = rospy.ServiceProxy(service_name, Control)     #client

        Tk.__init__(self)
        self.title("Keyboard Control Receiver")
        self.option_add('*tearOff', FALSE)
        self.bind("<Key>", self.callbackKey)
        self.bind("<KeyRelease>", self.callbackKey)

    def callbackKey(self, event):
        k = event.keysym.upper()
        print k
        response = self.controller(k, event.type)
        if response.message:
            print response.message

if __name__ == "__main__":
    app = Controller()
    app.mainloop()
