#!/usr/bin/env python

import rospy
import cv2
from cv_bridge import CvBridge, CvBridgeError
from marceline.srv import Control
from sensor_msgs.msg import Image
import serial
import struct
from collections import defaultdict
import numpy as np

VELOCITYCHANGE = 200
ROTATIONCHANGE = 300
service_name = 'keyboard'
node_name = 'marceline_driver'

connection = None

class Driver:
     # static variables for keyboard callback
    callbackKeyUp = False
    callbackKeyDown = False
    callbackKeyLeft = False
    callbackKeyRight = False
    callbackKeyLastDriveCommand = ''

    def __init__(self):

         # connecting
        print 'success'
        self.onConnect()
        self.sendCommandASCII('128') # set passive
        self.sendCommandASCII('131') # set safe
        # start node and spin.
        rospy.init_node(node_name)
        self.bridge = CvBridge()
        self.boundaries = self.createBoundaries()
        self.autoMode = False

        self.pub = rospy.Publisher('camera/visible/image', Image, queue_size=2)
        rospy.Subscriber("usb_cam/image_raw", Image, self.imageCallback)

        rospy.Service(service_name, Control, self.command_callback)

        self.rate = rospy.Rate(10) # 10hz
        self.run()

    def run(self):
    	while not rospy.is_shutdown():
            velocity = 0
            velocity += VELOCITYCHANGE if self.callbackKeyUp is True else 0
            velocity -= VELOCITYCHANGE if self.callbackKeyDown is True else 0
            rotation = 0
            rotation += ROTATIONCHANGE if self.callbackKeyLeft is True else 0
            rotation -= ROTATIONCHANGE if self.callbackKeyRight is True else 0

            # compute left and right wheel velocities
            vr = velocity + (rotation/2)
            vl = velocity - (rotation/2)

            # create drive command
            cmd = struct.pack(">Bhh", 145, vr, vl)
            if cmd != self.callbackKeyLastDriveCommand:
                self.sendCommandRaw(cmd)
                self.callbackKeyLastDriveCommand = cmd

            self.rate.sleep()

    def createBoundaries(self):
        
        boundaries = [
            # ('red', [0, 128, 148], [179, 211, 197]),        # red
            # ('orange', [5, 80, 132], [16, 228, 181]),       # orange
            ('orange', [8, 149, 128], [15, 253, 170]),       # orange
            ('yellow', [25, 121, 121], [30, 203, 162]),     # yellow
            #('light green', [29, 83, 132], [34, 140, 182]), # light green
            ('green', [73, 21, 121], [93, 66, 180]),       # green
            #('light blue', [0, 0, 130], [110, 35, 164]),    # light blue
            #('blue', [0, 6, 136], [177, 104, 218]),         # blue
            #('purple', [0, 48, 129], [179, 84, 201]),    # purple
            #('pink', [0, 0, 0], [174, 100, 165])
        ]
        return boundaries

    def timerRotationCallback(self, event):
        print 'in timer'
        self.callbackKeyLeft = False
        self.callbackKeyRight = False
        if self.autoMode:
            self.callbackKeyUp = True

    # Analyze the image and return the color
    def imageCallback(self, image):

        cv_rgb = self.bridge.imgmsg_to_cv2(image, "bgr8")
        cv_hsv = cv2.cvtColor(cv_rgb, cv2.COLOR_BGR2HSV)

        color = self.getColor(cv_hsv)
        print color

        if color == 'black':
            # turn left
            self.callbackKeyLeft = True
            if self.autoMode:
                self.callbackKeyUp = False
            rospy.Timer(rospy.Duration(1.5), self.timerRotationCallback, oneshot=True)

        elif color == 'white':
            # turn right
            self.callbackKeyRight = True
            if self.autoMode:
                self.callbackKeyUp = False
            rospy.Timer(rospy.Duration(1.5), self.timerRotationCallback, oneshot=True)

        else:
            self.singColor(color)

    def getColor(self, image):
        # determine black
        '''
        black_lower, black_upper = np.array([0, 0, 0]), np.array([255, 255, 100])
        black_mask = cv2.inRange(image, black_lower, black_upper)
        black_mask = np.asarray(black_mask)
        black_sum = np.sum(black_mask)
        #print 'black:' , int(black_sum)
        if black_sum > 60000000:
           return 'black'

        # determine white
        white_lower, white_upper = np.array([20, 0, 0]), np.array([50,255,154])
        white_mask = cv2.inRange(image, white_lower, white_upper)
        white_mask = np.asarray(white_mask)
        white_sum = np.sum(white_mask)
        #print 'white:' , int(white_sum)
        if white_sum > 60000000:
           return 'white'
        '''
        # determine other color
        max_pixel = 0
        dominant_color = 'unknown'
        
        for color, lower, upper in self.boundaries:
            lower = np.array(lower, dtype = "uint8")
            upper = np.array(upper, dtype = "uint8")
            color_mask = cv2.inRange(image, lower, upper)
            color_mask = np.asarray(color_mask)
            pixel_sum = np.sum(color_mask)

            if pixel_sum > max_pixel:
                max_pixel = pixel_sum
                dominant_color = color

                print color

        return dominant_color

    def singColor(self, color):
        tone = ''
        duration = '32'
        if color == 'green':      #C
            tone = '60'
        elif color == 'yellow': #D
            tone = '62'
        elif color == 'orange': #E
            tone = '64'
        #else: #G
         #   tone = '67'

        '''
        if color == 'red':      #C
            tone = '60'
        elif color == 'orange': #D
            tone = '62'
        elif color == 'yellow': #E
            tone = '64'
        elif color == 'light green':  #F
            tone = '65'
        elif color == 'green':   #G
            tone = '67'
        elif color == 'blue': #A
            tone = '69'
        elif color == 'purple': #B
            tone = '71'
        '''
        if tone != '':
            self.sendCommandASCII('140 3 1 '+ tone + ' ' + duration + ' 141 3')
        #else:
         #   self.sendCommandASCII('140 3 1 '+ '90' + ' ' + duration + ' 141 3') # debug use


    #getting command from controller
    def command_callback(self, request):
        k = request.keypress
        print k
        if request.keytype == '2': # KeyPress; need to figure out how to get constant
            if k == 'S': # MUSIC!
                self.playSong()
            elif k == 'A': #automatic move
                self.autoMode = True
                self.callbackKeyUp = True
            elif k == 'P':
                self.autoMode = False
                self.callbackKeyUp = False
            elif k == 'UP':
                self.callbackKeyUp = True
            elif k == 'DOWN':
                self.callbackKeyDown = True
            elif k == 'LEFT':
                self.callbackKeyLeft = True
            elif k == 'RIGHT':
                self.callbackKeyRight = True
            else:
                raise ValueError(repr(k), "not handled")
        elif request.keytype == '3': # KeyRelease; need to figure out how to get constant
            if k == 'UP':
                self.callbackKeyUp = False
            elif k == 'DOWN':
                self.callbackKeyDown = False
            elif k == 'LEFT':
                self.callbackKeyLeft = False
            elif k == 'RIGHT':
                self.callbackKeyRight = False
        return ''


    #control iRobot
    # sendCommandASCII takes a string of whitespace-separated, ASCII-encoded base 10 values to send
    def sendCommandASCII(self, command):
        cmd = ""
        for v in command.split():
            cmd += chr(int(v))

        self.sendCommandRaw(cmd)

    # sendCommandRaw takes a string interpreted as a byte array
    def sendCommandRaw(self, command):
        global connection

        try:
            if connection is not None:
                connection.write(command)
            else:
                print "Not connected."
        except serial.SerialException:
            print "Lost connection"
            connection = None

        #print ' '.join([ str(ord(c)) for c in command ])


    def onConnect(self):
        global connection

        port = "/dev/ttyUSB0"
        print "Connect to " + str(port) + "... "

        if connection is not None:
            print "You're already connected!"
            return

        try:
            connection = serial.Serial(port, baudrate=115200, timeout=1)
            print "Connected!"
        except:
            print "Failed."

    def playSong(self):
        self.sendCommandASCII('140 3 16'
                              ' 58 48' + # _Bb
                              ' 65 48' + # F
                              ' 63 8' + # Eb
                              ' 62 8' + # D
                              ' 60 8' + # C
                              ' 70 48' + # Bb
                              ' 65 24' + # F
                              ' 63 8' + # Eb
                              ' 62 8' + # D
                              ' 60 8' + # C
                              ' 70 48' + # Bb
                              ' 65 24' + # F
                              ' 73 8' + # Eb
                              ' 62 8' + # D
                              ' 63 8' + # Eb
                              ' 60 48' + # C
                              ' 141 3') # Play music

if __name__ == "__main__":
    driver = Driver()



