#include "ros/ros.h"
#include "math_two_nums/MathTwoNums.h"
#include "math_two_nums/Mode.h"
#include "std_srvs/Empty.h"

std::string mode = "ADDITION";

bool mathCallback(math_two_nums::MathTwoNums::Request  &req,
                  math_two_nums::MathTwoNums::Response &res)
{
	ROS_INFO("mode: %s", mode.c_str());
	ROS_INFO("request: x=%f, y=%f", req.a, req.b);

	/*********************************
	 *
	 * DO THE MATH BASED ON THE MODE
	 *
	 *********************************/
	if (mode == "ADDITION")
	{
		res.result = req.a + req.b;
	}
	else if(mode == "SUBTRACTION"){
		res.result = req.a - req.b;
	}
	else if( mode == "MULTIPLICATION"){
		res.result = req.a * req.b;
	} 
	else if(mode == "DIVISION"){
		if(req.b == 0){
			std::cout<< "can't be divided by 0" << std::endl;
		}
		else{
			res.result = req.a / req.b;
		}
	}
	
	ROS_INFO("sending back response: %f", res.result);
	return true;
}

bool exitCallback(std_srvs::Empty::Request &req,
                  std_srvs::Empty::Response &res)
{
	ros::shutdown();

	return true;
}

/******************************
 *
 * SETUP MODE CALLBACK HERE.
 *
 ******************************/
bool modeCallback(math_two_nums::Mode::Request &req, math_two_nums::Mode::Response &res){
		res.flag = true;
		char input = req.a;
		std::cout << input << std::endl;
		if (input == 'A' || input == 'a')
                {
                        mode = "ADDITION";
                }
                else if (input == 'S' || input == 's')
                {
                        mode = "SUBTRACTION";
                }
                else if (input == 'M' || input == 'm')
                {
                        mode = "MULTIPLICATION";
                }
                else if (input == 'D' || input == 'd')
                {
                        mode = "DIVISION";
                }

	return true;
}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "math_two_nums_server");
	ros::NodeHandle node;

	ros::ServiceServer mathServer = node.advertiseService("math_two_nums", mathCallback);
	ros::ServiceServer exitServer = node.advertiseService("exit", exitCallback);

	/******************************
	 *
	 * SETUP THE MODE SERVER HERE.
	 *
	 ******************************/
	ros::ServiceServer modeServer = node.advertiseService("mode", modeCallback);
	ros::ServiceServer modeExitServer = node.advertiseService("exit", exitCallback);

	ROS_INFO("Ready to math two nums.");

	ros::spin();

  return 0;
}
