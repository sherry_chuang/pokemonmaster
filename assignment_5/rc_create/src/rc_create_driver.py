#!/usr/bin/env python

import rospy
from rc_create.srv import Control
import serial
import struct

VELOCITYCHANGE = 200
ROTATIONCHANGE = 300
service_name = 'keyboard'
node_name = 'rc_create_driver'

connection = None

class Driver:
     # static variables for keyboard callback
    callbackKeyUp = False
    callbackKeyDown = False
    callbackKeyLeft = False
    callbackKeyRight = False
    callbackKeyLastDriveCommand = ''
    def __init__(self):

         # connecting
        print 'success'
        self.onConnect()

        self.sendCommandASCII('128') # set passive
        self.sendCommandASCII('131') # set safe
        # start node and spin.
        rospy.init_node(node_name)

        rospy.Service(service_name, Control, self.command_callback)

        self.rate = rospy.Rate(10) # 10hz
        self.run()

    def run(self):
        while not rospy.is_shutdown():
            velocity = 0
            velocity += VELOCITYCHANGE if self.callbackKeyUp is True else 0
            velocity -= VELOCITYCHANGE if self.callbackKeyDown is True else 0
            rotation = 0
            rotation += ROTATIONCHANGE if self.callbackKeyLeft is True else 0
            rotation -= ROTATIONCHANGE if self.callbackKeyRight is True else 0

            # compute left and right wheel velocities
            vr = velocity + (rotation/2)
            vl = velocity - (rotation/2)

            # create drive command
            cmd = struct.pack(">Bhh", 145, vr, vl)
            if cmd != self.callbackKeyLastDriveCommand:
                self.sendCommandRaw(cmd)
                self.callbackKeyLastDriveCommand = cmd

            self.rate.sleep()

    #getting command from controller
    def command_callback(self, request):
        k = request.keypress
        print k
        if request.keytype == '2': # KeyPress; need to figure out how to get constant
            if k == 'S': # MUSIC!
                self.playSong()
            elif k == 'UP':
                self.callbackKeyUp = True
            elif k == 'DOWN':
                self.callbackKeyDown = True
            elif k == 'LEFT':
                self.callbackKeyLeft = True
            elif k == 'RIGHT':
                self.callbackKeyRight = True
            else:
                raise ValueError(repr(k), "not handled")
        elif request.keytype == '3': # KeyRelease; need to figure out how to get constant
            if k == 'UP':
                self.callbackKeyUp = False
            elif k == 'DOWN':
                self.callbackKeyDown = False
            elif k == 'LEFT':
                self.callbackKeyLeft = False
            elif k == 'RIGHT':
                self.callbackKeyRight = False
        return ''


    #control iRobot
    # sendCommandASCII takes a string of whitespace-separated, ASCII-encoded base 10 values to send
    def sendCommandASCII(self, command):
        cmd = ""
        for v in command.split():
            cmd += chr(int(v))

        self.sendCommandRaw(cmd)

    # sendCommandRaw takes a string interpreted as a byte array
    def sendCommandRaw(self, command):
        global connection

        try:
            if connection is not None:
                connection.write(command)
            else:
                print "Not connected."
        except serial.SerialException:
            print "Lost connection"
            connection = None

        print ' '.join([ str(ord(c)) for c in command ])


    def onConnect(self):
        global connection

        port = "/dev/ttyUSB0"
        print "Connect to " + str(port) + "... "

        if connection is not None:
            print "You're already connected!"
            return

        try:
            connection = serial.Serial(port, baudrate=115200, timeout=1)
            print "Connected!"
        except:
            print "Failed."

    def playSong(self):
        self.sendCommandASCII('140 3 16'
                              ' 58 48' + # _Bb
                              ' 65 48' + # F
                              ' 63 8' + # Eb
                              ' 62 8' + # D
                              ' 60 8' + # C
                              ' 70 48' + # Bb
                              ' 65 24' + # F
                              ' 63 8' + # Eb
                              ' 62 8' + # D
                              ' 60 8' + # C
                              ' 70 48' + # Bb
                              ' 65 24' + # F
                              ' 73 8' + # Eb
                              ' 62 8' + # D
                              ' 63 8' + # Eb
                              ' 60 48' + # C
                              ' 141 3') # Play music

if __name__ == "__main__":
    driver = Driver()



